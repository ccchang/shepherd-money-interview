package com.shepherdmoney.interviewproject.vo.request;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CreateUserPayload {

    private String name;

    private String email;
}
