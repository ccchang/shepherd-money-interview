package com.shepherdmoney.interviewproject.controller;

import com.shepherdmoney.interviewproject.model.BalanceHistory;
import com.shepherdmoney.interviewproject.model.CreditCard;
import com.shepherdmoney.interviewproject.model.User;
import com.shepherdmoney.interviewproject.repository.CreditCardRepository;
import com.shepherdmoney.interviewproject.repository.UserRepository;
import com.shepherdmoney.interviewproject.vo.request.AddCreditCardToUserPayload;
import com.shepherdmoney.interviewproject.vo.request.UpdateBalancePayload;
import com.shepherdmoney.interviewproject.vo.response.CreditCardView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@RestController
public class CreditCardController {
    @Autowired
    CreditCardRepository creditCardRepository;

    @Autowired
    UserRepository userRepository;

    /**
     *  Create a credit card entity, and then associate that credit card with user with given userId.
     */
    @PostMapping("/credit-card")
    public ResponseEntity<Integer> addCreditCardToUser(@RequestBody AddCreditCardToUserPayload payload) {
        Optional<User> user = userRepository.findById(payload.getUserId());

        // Return 400 BAD_REQUEST code if the user doesn't exist.
        if (user.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        // Creat a new credit card associated with the given user.
        CreditCard creditCard = new CreditCard();
        creditCard.setUser(user.get());
        creditCard.setNumber(payload.getCardNumber());
        creditCard.setIssuanceBank(payload.getCardIssuanceBank());

        // Store the credit card persistently.
        creditCardRepository.save(creditCard);

        // Return 200 OK with the credit card id if the user exists and credit card is successfully associated with the user.
        return ResponseEntity.status(HttpStatus.OK).body(creditCardRepository.save(creditCard).getId());
    }

    /**
     * Get a list of all credit cards associated with the given userId.
     */
    @GetMapping("/credit-card:all")
    public ResponseEntity<List<CreditCardView>> getAllCardOfUser(@RequestParam int userId) {
        Optional<User> user = userRepository.findById(userId);

        // Return 400 BAD_REQUEST code and an empty list if the user doesn't exist.
        if (user.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new LinkedList<>());
        }

        // Convert user's credit cards to a list of CreditCardView.
        List<CreditCardView> creditCardViews = user.get().getCreditCards().stream()
                .map(creditCard -> new CreditCardView(creditCard.getIssuanceBank(), creditCard.getNumber()))
                .collect(Collectors.toList());

        // Return 200 OK with a list of all credit card associated with the user if the user exists.
        return ResponseEntity.status(HttpStatus.OK).body(creditCardViews);
    }

    /**
     * Given a credit card number, find a user associated with that credit card.
     */
    @GetMapping("/credit-card:user-id")
    public ResponseEntity<Integer> getUserIdForCreditCard(@RequestParam String creditCardNumber) {
        Optional<CreditCard> creditCard = creditCardRepository.findAll().stream().filter(card -> card.getNumber().equals(creditCardNumber)).findFirst();

        // Return 400 BAD_REQUEST if the credit card with the given credit card number doesn't exist.
        if (creditCard.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        // Return 400 BAD_REQUEST if there is no user associated with the given credit card.
        if (creditCard.get().getUser() == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        // Return 200 OK and the user id if there is a user associated with the given credit card.
        return ResponseEntity.status(HttpStatus.OK).body(creditCard.get().getUser().getId());
    }

    /**
     * Given a list of transactions, update credit cards' balance history.
     *
     * For example: if today is 4/12, a credit card's balanceHistory is [{date: 4/12, balance: 110}, {date: 4/10, balance: 100}],
     * Given a transaction of {date: 4/10, amount: 10}, the new balanceHistory is
     * [{date: 4/12, balance: 120}, {date: 4/11, balance: 110}, {date: 4/10, balance: 110}]
     */
    @PostMapping("/credit-card:update-balance")
    public ResponseEntity<String> postMethodName(@RequestBody UpdateBalancePayload[] payload) {

        List<CreditCard> creditCards = creditCardRepository.findAll();

        // Check if all credit card numbers of the given transactions exist.
        for (UpdateBalancePayload balancePayload: payload) {
            // Check if the credit card for the transaction exists.
            Optional<CreditCard> creditCard = creditCards.stream().filter(card -> card.getNumber().equals(balancePayload.getCreditCardNumber())).findFirst();

            // Return 400 Bad Request if the given card number is not associated with a card.
            if (creditCard.isEmpty()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(String.format("credit card number %s doesn't exist", balancePayload.getCreditCardNumber()));
            }
        }

        // All credit card numbers in the transactions exist, update the balance history for these credit cards.
        for (UpdateBalancePayload balancePayload: payload) {
            CreditCard creditCard = creditCards.stream().filter(card -> card.getNumber().equals(balancePayload.getCreditCardNumber())).findFirst().get();
            Optional<BalanceHistory> balanceHistory = creditCard.getBalanceHistories().stream().filter(history -> history.getDate().equals(balancePayload.getTransactionTime())).findFirst();

            // Create a new balance history entry if the transaction time appears for the first time.
            if (balanceHistory.isEmpty()) {
                BalanceHistory newBalanceHistory = new BalanceHistory();
                newBalanceHistory.setDate(balancePayload.getTransactionTime());
                newBalanceHistory.setBalance(balancePayload.getTransactionAmount());
                newBalanceHistory.setCreditCard(creditCard);
                creditCard.getBalanceHistories().add(newBalanceHistory);
            }
            // Else, update the previous balance history entry if the transaction time exists.
            else {
                double oldBalance = balanceHistory.get().getBalance();
                balanceHistory.get().setBalance(oldBalance + balancePayload.getTransactionAmount());
            }

            // Store the update persistently.
            creditCardRepository.save(creditCard);
        }

        // Return 200 OK if update is done and successful.
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
