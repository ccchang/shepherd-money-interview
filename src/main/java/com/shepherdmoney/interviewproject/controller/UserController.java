package com.shepherdmoney.interviewproject.controller;

import com.shepherdmoney.interviewproject.model.User;
import com.shepherdmoney.interviewproject.repository.UserRepository;
import com.shepherdmoney.interviewproject.vo.request.CreateUserPayload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
public class UserController {

    @Autowired
    UserRepository userRepository;

    /**
     * Create an user entity with information given in the payload.
     */
    @PutMapping("/user")
    public ResponseEntity<Integer> createUser(@RequestBody CreateUserPayload payload) {
        // Create a new user with the given name and email.
        User user = new User();
        user.setName(payload.getName());
        user.setEmail(payload.getEmail());

        // Return 200 OK with the user id.
        return ResponseEntity.status(HttpStatus.OK).body(userRepository.save(user).getId());
    }

    /**
     * Delete the user with the given user id.
     */
    @DeleteMapping("/user")
    public ResponseEntity<String> deleteUser(@RequestParam int userId) {
        // Return 400 Bad Request if a user with the ID does not exist
        if (userRepository.findById(userId).isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("user doesn't exist");
        }

        // Delete user.
        userRepository.deleteById(userId);

        // Return 200 OK if a user with the given ID exists, and the deletion is successful.
        return ResponseEntity.status(HttpStatus.OK).body("user is deleted successfully");
    }
}
