package com.shepherdmoney.interviewproject.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.*;

import java.util.LinkedList;
import java.util.List;

@Entity
@Table
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class CreditCard {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column
    private String issuanceBank;

    @Column
    private String number;

    @JsonBackReference
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="user_id")
    private User user;

    // Credit card's balance history. It is a requirement that the dates in the balanceHistory
    // list must be in chronological order, with the most recent date appearing first in the list.
    // Additionally, the first object in the list must have a date value that matches today's date,
    // since it represents the current balance of the credit card. For example:
    // [
    // {date: '2023-04-13', balance: 1500},
    // {date: '2023-04-12', balance: 1200},
    // {date: '2023-04-11', balance: 1000},
    // {date: '2023-04-10', balance: 800}
    // ]
    @JsonManagedReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "creditCard")
    @EqualsAndHashCode.Exclude
    @OrderBy("date DESC")
    private List<BalanceHistory> balanceHistories = new LinkedList<>();
}
